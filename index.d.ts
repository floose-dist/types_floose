/// <reference types="node" />
import ReadableStream = NodeJS.ReadableStream;

/*
 * Core classes
 */
export declare class Floose {
    private constructor();
    static run(config: Framework.Configuration): Promise<void>;
}

/*
 * Components
 */
export declare namespace Managers {

    export class ServerManager {
        private constructor();
        static getInstance(): ServerManager;
        get<T extends Framework.Server>(type: {prototype: T; }): T;
        getAll(): Framework.Server[];
    }

    // TODO: maybe remove from d.ts
    export class ModuleManager {
        private constructor();
        static getInstance(): ModuleManager;
        get<T extends Framework.Module>(type: {prototype: T; }): T;
        getAll(): Framework.Module[];
    }

    export class DriverManager {
        private constructor();
        static getInstance(): DriverManager;
        get<T extends Framework.Driver>(type: {prototype: T; }, connection: string): T;
        getAll(): Framework.Driver[];
        addConnection<T extends Framework.Driver>(connection: string, type: {prototype: T}, configuration: Framework.ComponentConfiguration): Promise<T>
        removeConnection(connection: string, type: {prototype: Framework.Driver}): void;
    }

    export class EventManager {
        private constructor();
        static getInstance(): EventManager;
        addListener(eventCode: string, listener: Framework.Listener): string;
        removeListener(identifier: string): void;
        dispatch(event: Framework.Event): Promise<void>;
    }

    export class CronManager {
        private constructor();
        static getInstance(): CronManager;
        stop(): Promise<void>;
        addSchedule(interval: string, task: Framework.Task): string;
        removeSchedule(identifier: string): void;
    }
}

/*
 * Framework
 */
export declare namespace Framework {

    import Schema = Utils.Validation.Schema;

    export abstract class Server {
        protected constructor();
        abstract readonly configurationValidationSchema: Schema;
        abstract init(config: ComponentConfiguration): Promise<void>;
        abstract start(): Promise<void>;
        abstract stop(): Promise<void>;
        restart(): Promise<void>;
        abstract readonly running: boolean;
    }

    export abstract class Driver {
        protected constructor();
        abstract readonly configurationValidationSchema: Schema;
        abstract init(config: ComponentConfiguration): Promise<void>;
    }

    export abstract class Module {
        protected constructor();
        abstract readonly configurationValidationSchema: Schema;
        abstract init(config: ComponentConfiguration): Promise<void>;
        abstract run(): Promise<void>;
        readonly name: string;
    }

    export interface Task {
        execute(): Promise<void>;
    }

    export interface Event {
        code: string;
        data?: {[key:string]: any};
    }
    export interface Listener {
        execute(event: Event): Promise<void>;
    }

    export interface Configuration {
        logging: {
            mode: "console"|"file"|"all"|"none";
            level: "debug"|"info"|"warn"|"error"|"fatal";
            rotation?: {
                interval?: string;
                retention?: number;
            }
        };
        storage: {
            path: string
        };
        driver: {[name: string]: {[connectionName: string]: {}}};
        module: {[name: string]: {}};
        server: {[name: string]: {}};
    }
    export interface ComponentConfiguration {
    }

    export namespace Data {
        export enum FilterType {
            EQ = "eq",
            NEQ = "neq",
            LIKE = "like",
            NLIKE = "nlike",
            IN = "in",
            NIN = "nin",
            IS = "is",
            NULL = "null",
            NOTNULL = "notnull",
            GT = "gt",
            LT = "lt",
            GTEQ = "gteq",
            LTEQ = "lteq",
            FINDSET = "finset",
            REGEXP = "regexp",
            FROM = "from",
            TO = "to",
            NTOA = "ntoa"
        }
        export interface Filter {
            table?: string;
            field: string;
            value?: string | number | boolean | undefined | any;
            type?: FilterType;
        }
        export interface FilterGroup {
            filters: Filter[];
        }

        export enum SortOrderDirection {
            ASC = "ASC",
            DESC = "DESC"
        }
        export interface SortOrder {
            table?: string;
            field: string;
            direction?: SortOrderDirection;
        }

        export interface SearchCriteria {
            filterGroups: FilterGroup[];
            sortOrders?: SortOrder[];
            pageSize?: number;
            currentPage?: number;
        }

        export enum JoinType {
            INNER = "INNER",
            LEFT = "LEFT",
            LEFT_OUTER = "LEFT OUTER",
            RIGHT = "RIGHT",
            RIGHT_OUTER = "RIGHT OUTER",
            FULL = "FULL",
            FULL_OUTER = "FULL OUTER"
        }
        export interface Join {
            joinType: JoinType;
            joinTable?: string;
            joinField: string;
            referenceTable: string;
            referenceTableAlias?: string;
            referenceField: string;
            selectReferenceFields?: string[];
        }

        export abstract class Model {
            protected _rowData: {
                [index: string]: any;
            };
            load(rowData: {
                [index: string]: any;
            }): Model;
            toObject(): {
                [index: string]: any;
            };
        }
        export abstract class Repository<T extends Model> {
            protected abstract _modelClass(): {new(): T};
            protected abstract _find(searchCriteria: SearchCriteria, joinCriteria?: Join[]): Promise<T[]>;
            abstract get(identifier: any, forceReload?: boolean): Promise<T>;
            abstract save(modelInterface: T): Promise<T>;
            abstract delete(modelInterface: T | T[]): Promise<void>;
            create(): T;
            find(filter: Filter[] | Filter, sortOrder?: SortOrder[] | SortOrder, pageSize?: number, currentPage?: number, join?: Join[] | Join): Promise<T[]>;
            find(filterGroup: FilterGroup[] | FilterGroup, sortOrder?: SortOrder[] | SortOrder, pageSize?: number, currentPage?: number, join?: Join[] | Join): Promise<T[]>;
            find(searchCriteria: SearchCriteria, join?: Join[] | Join): Promise<T[]>;
        }
    }
}

/*
 * Utils
 */
export declare namespace Utils {

    export class Logger {
        static debug(content: any): void;
        static info(content: any): void;
        static warn(content: any): void;
        static error(content: any): void;
        static fatal(content: any): void;
    }

    export class Storage {
        static getFullPath(relativePath?: string): string;
        static write(path: string, contents: any, append?: boolean): Promise<void>;
        static writeSync(path: string, contents: any, append?: boolean): void;
        static writeStream(path: string, stream: ReadableStream, append?: boolean): Promise<void>;
        static writeStreamSync(path: string, stream: ReadableStream, append?: boolean): void;
    }

    export namespace Validation {
        export enum SchemaType {
            STRING = "string",
            BOOLEAN = "boolean",
            NUMBER = "number",
            DATE = "date",
            ARRAY = "array",
            OBJECT = "object"
        }
        export enum StringFormat {
            ALPHANUM = "alphanum",
            TOKEN = "token",
            EMAIL = "email",
            IP = "ip",
            URI = "uri",
            GUID = "guid",
            HEX = "hex",
            HOSTNAME = "hostname",
            ISO_DATE = "isoDate",
            BASE64 = "base64",
            CREDIT_CARD = "creditCard"
        }
        interface StringSchema {
            type: SchemaType.STRING;
            min?: number;
            max?: number;
            length?: number;
            format?: StringFormat;
            regExp?: RegExp;
            allowedValues?: string[];
        }
        interface NumberSchema {
            type: SchemaType.NUMBER;
        }
        interface BooleanSchema {
            type: SchemaType.BOOLEAN;
        }
        interface DateSchema {
            type: SchemaType.DATE;
        }
        interface ArraySchema {
            type: SchemaType.ARRAY;
            min?: number;
            max?: number;
            length?: number;
            itemSchema?: Schema | Schema[];
        }
        interface ObjectSchema {
            type: SchemaType.OBJECT;
            requiredKeys?: string[];
            itemSchema?: {
                [key: string]: Schema | Schema[];
            };
        }
        export type Schema = StringSchema | ArraySchema | NumberSchema | BooleanSchema | DateSchema | ObjectSchema;

        export class Validator {
            static validate(data: any, schema: Schema): Promise<void>;
            static validateWithJsonSchema(data: any, schema: any): Promise<void>;
        }
        export class ValidationError extends Error {
            constructor(message?: string);
        }
    }

    export namespace HTTP {
        export class Request {
            static get(options: {
                headers: {
                    [index: string]: string;
                };
                url: string;
                responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'; // Automatically parses the response body as JSON string (default: json)
                timeout?: number; // Timeout in milliseconds (default: 120000)
                params: { [index: string]: any; }
            }): Promise<RequestResponse>;

            static post(options: {
                headers: {
                    [index: string]: string;
                };
                url: string;
                responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'; // Automatically parses the response body as JSON string (default: json)
                timeout?: number; // Timeout in milliseconds (default: 120000)
                body?: string | Buffer | {[index: string]: any};
            }): Promise<RequestResponse>;

            static put(options: {
                headers: {
                    [index: string]: string;
                };
                url: string;
                responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'; // Automatically parses the response body as JSON string (default: json)
                timeout?: number; // Timeout in milliseconds (default: 120000)
                body: string | Buffer | {[index: string]: any};
            }): Promise<RequestResponse>;

            static patch(options: {
                headers: {
                    [index: string]: string;
                };
                url: string;
                responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'; // Automatically parses the response body as JSON string (default: json)
                timeout?: number; // Timeout in milliseconds (default: 120000)
                body: string | Buffer | {[index: string]: any};
            }): Promise<RequestResponse>;

            static delete(options: {
                headers: {
                    [index: string]: string;
                };
                url: string;
                responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'; // Automatically parses the response body as JSON string (default: json)
                timeout?: number; // Timeout in milliseconds (default: 120000)
            }): Promise<RequestResponse>;
        }
        export enum RequestMethod {
            GET = 'GET',
            POST = 'POST',
            PUT = 'PUT',
            PATCH = 'PATCH',
            DELETE = 'DELETE'
        }
        export interface RequestResponse {
            statusCode: number;
            statusMessage: string;
            headers: {[index: string]: string};
            body: any;
        }
    }
}